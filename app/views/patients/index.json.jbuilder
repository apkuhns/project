json.array!(@patients) do |patient|
  json.extract! patient, :id, :patient_id, :name
  json.url patient_url(patient, format: :json)
end
