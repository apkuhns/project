json.array!(@tests) do |test|
  json.extract! test, :id, :reasons
  json.url test_url(test, format: :json)
end
