class CreatePhysicians < ActiveRecord::Migration
  def change
    create_table :physicians do |t|
      t.integer :physician_id
      t.string :name

      t.timestamps
    end
  end
end
